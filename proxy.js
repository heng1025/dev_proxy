const devConfig = require("./config.js");
/* 代理跨域访问（类似nginx反向代理）*/
const proxyMiddleware = require('http-proxy-middleware');
let proxyOption = null; 
if (devConfig.envUrlSet) {
    /* 炒股大赛接口代理 */
    proxyOption = proxyMiddleware(['/match', '/new_year', '/apply'], {
        target: devConfig.target.cgMatch
    });
} else {
    /* 游戏接口代理 */
    proxyOption = proxyMiddleware(['/passport', '/connect'], {
        target: devConfig.target.game_stock
    });
}
module.exports = proxyOption;
