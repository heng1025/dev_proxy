const gulp = require('gulp');
/* 压缩js */
const uglify = require('gulp-uglify');
/* 编译ts */
const ts = require("gulp-typescript");
/* 产生映射文件(不会生成具体文件) */
const sourcemaps = require('gulp-sourcemaps');
/* 使用配置文件 */
const tsProject = ts.createProject('tsconfig.json');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
/* 读取配置文件 */
const devConfig = require("./config.js");
/* 读取代理配置文件 */
const proxyOption = require("./proxy.js");

let rootUrl = '';
let indexSrc = '';
let startPath = '';
if (devConfig.envUrlSet) {
    rootUrl = devConfig.dev_RootDir.cgMatch;
    indexSrc = 'chaogu.html';
    startPath = '/chaogu.html?userId=U000023348';
} else {
    rootUrl = devConfig.dev_RootDir.game_stock;
    indexSrc = 'index.html';
    startPath = '/game/index.html';
}
/* 配置监听文件路径 */
const htmlRoot = rootUrl + "*.html";
const cssRoot = rootUrl + "**/css/*.css";
const jsRoot = rootUrl + "**/js/*.js";
const tsRoot = rootUrl + "ts/*.ts";

/* 静态服务器 + 监听 scss/html 文件 */
gulp.task('serve', () => {
    /* 开启静态服务器 */
    browserSync.init({ 
        /* 指定服务器路径 */
        server: {
            baseDir: rootUrl,
            /* 首页名称 */
            index: indexSrc,
            /* 自定义中间件进行代理 */
            //middleware: [proxyOption]
        },
        /*设置端口（默认3000）*/
        // port: 80,
        /* 确定网络连接 */
        online: true,
        /* 开启https */
        /* https: true,*/
        /* 开始路径 */
       // startPath: startPath,
        /* 指定打开的浏览器 */
        browser: ["chrome"],
        /* 默认打开的页面 */
        open: "external"
    });
    /* 自动编译 */
    gulp.watch(tsRoot, ['ts']).on('change', reload);
    gulp.watch(jsRoot).on('change', reload);
    gulp.watch(cssRoot).on('change', reload);
    gulp.watch(htmlRoot).on('change', reload);
});

gulp.task('watchTs',function(){
     gulp.watch(tsRoot, ['ts']);
});
/* 编译ts,生成js并压缩 */
gulp.task("ts", () => {
    var tsResult = gulp.src(tsRoot)
        .pipe(sourcemaps.init()) // generate sourcemap
        .pipe(tsProject());
    return tsResult.js
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(rootUrl + 'js'));
});

/* 开发环境 */
gulp.task('default', ['serve']);
/* 上线部署 */
gulp.task("publish", () => {

});
